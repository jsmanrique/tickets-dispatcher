# Tickets dispatcher

Given the number of raffle participants and the number of tickets to raflle, this software generate the list of fortunate candidates' numbers.

## Requirements

You must have git and Python installed.

The software runs in your CLI (Command Language Interface).

## Installation


Clone this repo:
```
clone https://gitlab.com/jsmanrique/tickets-dispatcher
```

## Usage

Go to the created directory and run the software, providing required parameters:
```
cd tickets-dispatcher
python tickets.py -p 10 -t 2
```

If you have any doubt about the parameters, you can run:
```
python tickets.py --help
```

## Contributing

If you find an issue or a potential improvement, start by filling an issue, and let's discuss on it.


## Authors and acknowledgment

Author: J. Manrique <jsmanrique@gmail.com>

## License

GPL v3