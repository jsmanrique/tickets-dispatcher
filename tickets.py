# tickets.py
# Copyright (C) 2023 J. Manrique Lopez de la Fuente
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#!/usr/bin/env python
# coding: utf-8

"""tickets.py: given a number of participants and the number of tickets
 to give away, it provides a ramdom sample of numbers to give them a ticket
"""

__author__ = "J. Manrique Lopez de la Fuente"
__copyright__ = "Copyright (C) 2023, J. Manrique Lopez de la Fuente"
__license__ = "GPL"

__version__ = "0.0.1"
__maintainer__ = "J. Manrique Lopez de la Fuente"

import random

import argparse
#import configparser

def parse_args(args):
	parser = argparse.ArgumentParser(description = "Get the set of people to give them a ticket")
	parser.add_argument('-p', '--participants', dest= 'participants', help = 'Number of participants', required = True)
	parser.add_argument('-t', '--tickets', dest = 'tickets', help = 'Number of tickets', required = True)

	return parser.parse_args()

def raffle(participants, tickets):
	p = int(participants)
	t = int(tickets)
	fortunate = random.sample(range(1,p), t)
	fortunate.sort()

	return fortunate

def main(args):
	args = parse_args(args)

	print(raffle(args.participants, args.tickets))

if __name__ == '__main__':
	import sys
	main(sys.argv[1:])
